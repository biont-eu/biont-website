---
custom_css: markdown
title: Training
layout: markdown_table

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http://biont-training.eu/training/training
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "Calendar of training events offered by BioNT "
  "keywords": "Research Data Management, Electronic Lab Notebooks, Data Publication, Software Publication, Databases, Semantics, Ontologies, Programming, Data Science, Machine Learning, Software Development Tools, Data Generation, Analysis Protocols, Genomics, Metagenomics, Transcriptomics, Metatranscriptomics, Proteomics, Metaproteomics, Metabolomics, Microbial Bioinformatics, Microbial Pathogenesis, Evolution, Multi-Omics, Image Analysis, Infrastructure, Workflows, Software Frameworks, Virtual Machines, Software Containers, Software Orchestrators, Workflow Management Systems, Cloud Computing, Train-the-trainer"
---

Here you find the list of BioNT training offered in the upcoming months. 
Add this page to your bookmarks to stay updated as we provide the registration links!
{: .my-5}

|Title | Organizer | Host   | :date: Date(s) | Time |Type*| Event website / registration link
| --------  | ------------------- | --------------------- |
| A practical introduction to bioinformatics and RNA-seq using Galaxy | University of Freiburg | Remote | September 4, 2023 - September 8, 2023 | 9:00 - 13:00 GMT+02:00 | Open for registration | <a href="https://www.cecam.org/workshop-details/1260"><i class="fa-solid fa-arrow-up-right-from-square"></i></a> |  
| Introduction to programming languages | University of Freiburg | Remote | Nov 2023 | to be decided | In progress | |  
| Command-line and cluster computing | HPCNOw | Remote | Jan 2024 | to be decided | In progress | |
| Open and FAIR principles and Data management | ZBMED | Remote | Mar 2024 | to be decided | In progress |  |
{: .container .mb-5 .md .news_item .DataTableSorted}

