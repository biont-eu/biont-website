---
layout: markdown_table
title: About
permalink: /about
---

{: .mb-1 .mt-5}
<b>Background</b>

BioNT is an international consortium of nine partners, including six academic entities and three Small and Medium enterprises (SMEs).
The vision of the consortium is to provide a high-quality training program and community for digital skills
relevant to the biotechnology industry and biomedical sector.

{: .mb-1 .mt-5}
<b>Mission</b>

The training model designed by the project proponents encodes the following missions:
- To provide high-quality courses in the context of two coherently designed curricula; the first
aimed at enhancing the basic digital skills coverage of staff and job seekers in the
healthcare and biotechnology fields, and the second at empowering technological leaders
and innovators.
- To have a beneficial impact not only on the course participants, but also on their communities,
supporting the development of basic levels of digital intensity in SMEs and businesses in
the focus sectors.
- To ensure the community and activities sustainability beyond the project duration, through the
empowerment of the individuals involved in the project, as well as of their business-specific,
sector-specific, language-specific, etc. communities. By treating the training content itself in a
FAIR way, we can generate communities around the training program using collaborative
approaches (such as social coding) to create sustainably maintained pedagogical content.


{: .mb-1 .mt-5}
<b>Work Packages</b>

<img class="img-fluid mb-5" src="{{ 'assets/img/work_program/WPs.png' | relative_url }}" alt="Overview of Work Packages">
{: style="float: right;margin-left:20px"}