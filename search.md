---
title: Search
layout: default
---


<div class="container" style="margin-top:12rem;margin-bottom:20rem;">

<form class="d-flex">
<input id="search-input" class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
 </form>
 <div class="list-group col-lg-3 col-md-6 search_result" id="results-container">
 </div>
</div>
