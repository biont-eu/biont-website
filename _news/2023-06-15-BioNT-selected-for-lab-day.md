---
title: "BioNT at the EMBL Lab Day 2023 event"
custom_css: markdown
category: General
category-color: "#1C6E8C"
author: Silvia Di Giorgio
date: 2023-06-15
description: " BioNT at the EMBL Lab Day 2023 event  ... "
layout: news_article
---

We are happy to announce that the BioNT project has been selected for presenting a poster at the annual EMBL Lab Day 2023. 
