---
title: "BioNT has been selected for funding"
custom_css: markdown
category: General
category-color: "#1C6E8C"
author: Silvia Di Giorgio
date: 2022-09-1
description: " We are happy to announce that the BioNT project has been selected for funding  ... "
layout: news_article
---

We are happy to announce that the BioNT project has been selected for funding. 
The DIGITAL Europe Programme call [DIGITAL-2022-TRAINING-02-SHORT-COURSES](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/digital-2022-training-02-short-courses) aims at 
increasing the training offer in digital skills of people in the workforce and job-seekers, through free and open short-term courses. The project got funded and will start its activities in early 2023.