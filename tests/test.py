#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
import pytest
from selenium import webdriver
import sys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

class TestClass():

    @classmethod
    def setup_class(cls):
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.driver = webdriver.Chrome(chrome_options=options)


    @classmethod
    def teardown_class(cls):
        cls.driver.close()

    def test_homepage(cls):
        cls.driver.get('file:///builds/nfdi4microbiota/nfdi4microbiota_pages/test/index.html')
        title = "NFDI4Microbiota-Home"
        assert title == cls.driver.title

    def test_search(cls):
        cls.driver.get('file:///builds/nfdi4microbiota/nfdi4microbiota_pages/test/index.html')
        assert "results-container" in cls.driver.page_source

    def test_404_exists(cls):
        cls.driver.get('file:///builds/nfdi4microbiota/nfdi4microbiota_pages/test/404.html')
        assert "404 Page" == cls.driver.title
    
    def test_about(cls):
        cls.driver.get('file:///builds/nfdi4microbiota/nfdi4microbiota_pages/test/about/index.html')
        assert "About" == cls.driver.title
    
    # Test consortium pages
    def test_consortium_interaction(cls):
        cls.driver.get('file:///builds/nfdi4microbiota/nfdi4microbiota_pages/test/consortium/interaction.html')
        assert "Interactions with other NFDI consortia" == cls.driver.title
    
    def test_consortium_partners(cls):
        cls.driver.get('file:///builds/nfdi4microbiota/nfdi4microbiota_pages/test/consortium/international-partners.html')
        assert "International networks" == cls.driver.title
    
    def test_consortium_participants(cls):
        cls.driver.get('file:///builds/nfdi4microbiota/nfdi4microbiota_pages/test/consortium/participants.html')
        assert "Participants" == cls.driver.title
    
    def test_consortium_interaction(cls):
        cls.driver.get('file:///builds/nfdi4microbiota/nfdi4microbiota_pages/test/consortium/partners.html')
        assert "Partners" == cls.driver.title
    
    def test_consortium_interaction(cls):
        cls.driver.get('file:///builds/nfdi4microbiota/nfdi4microbiota_pages/test/consortium/supporting-societies.html')
        assert "Professional-societies" == cls.driver.title
