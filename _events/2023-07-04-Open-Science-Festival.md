---
title: Poster presentation at the Open Science Festival
authors: all partners
type: Poster presentation
event: Open Science Festival
location: Cologne
date: 2023-07-04
info: https://www.zbmed.de/en/networking/events/open-science-festival
month: 'JUL'
day: '04'
---
