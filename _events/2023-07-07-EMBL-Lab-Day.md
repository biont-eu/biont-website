---
title: Poster presentation at EMBL Lab Day 2023
authors: all partners
type: Poster presentation
event: EMBL Lab Day 2023
location: Heidelberg
date: 2023-07-07
info: https://www.embl.org/events/
month: 'JUL'
day: '07'
---