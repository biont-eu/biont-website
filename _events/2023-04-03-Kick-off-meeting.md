---
title: BioNT Kick-Off Meeting
authors: all partners 
type: Presentations followed by discussions
event: 1st BioNT meeting
location: Zoom
date: 2023-04-03
info: https://www.linkedin.com/company/90760782/feed/posts/
month: 'APR'
day: '03'
---

