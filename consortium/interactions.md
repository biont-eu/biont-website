---
custom_css: markdown
title: Interactions with other Communities
layout: markdown_table
# permalink: /consortium/:title

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http:/biont-trainig/.eu/community/interactions
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "List of training communities "
  "keywords": ""
---

<img class="img-fluid mb-5" src="{{ 'assets/img/synergies/thecarpentries.png' | relative_url }}" width="400" height="200">

{: .col-6}


<img class="img-fluid mb-5" src="{{ 'assets/img/synergies/galaxy.jpg' | relative_url }}" width="400" height="200">
{: .col-6}



<img class="img-fluid mb-5" src="{{ 'assets/img/synergies/Elixir-Europe-logo-1.png' | relative_url }}" width="200" height="100">
{: .col-6}

{: .mb-5}

