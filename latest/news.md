---
custom_css: markdown
title: News
layout: news

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": https://biont-training.eu/latest/news
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "News regarding BioNT"
---



