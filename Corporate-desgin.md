# Corporate Design for the BioNT Central Web Portal

To have a consistent and unified design on the web portal, it was agreed on the following corporate design. 
This is of importance, as the user's eye needs to be pleased, so he can not only find the information easily but also is not exhausted by inconsistent design throughout the page.

## Page design
- Same width for all pages
- Uniform buttons: rounded edges like on the first page, no frame, blue buttons with white font  

## Font
Font type: Roboto
Font colors: black, blu, white
Font Sizes:
- Heading: H2, use '##'
- Subheading: H3, use '###'
...if more subheadings are needed, use H4, H5, etc.
- Text: 11
- Underlines are only used for links, headlines are not underlined

## Tables and accordion
- Same display of tables: same width, same design
- Avoid a column, where only a link icon is displayed, the link could be made to a text instead (avoid redundancy)
- No blue marking of the accordion cells if selected or not

## Links
- Color of the links is orange
- All links are underlined.
- If the links have hovered, they will turn bold
- Internal links are opened in the same tab.
- External links are opened in a new tab, use '[link](url){:target=”_blank”}'

## Content and language
- Introduction sentences for each page
- Use American English
- Use [Grammarly](https://app.grammarly.com/) for grammar checks and [DeepL](https://www.deepl.com/de/translator) for translation
