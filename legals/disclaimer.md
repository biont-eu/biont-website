---
custom_css: markdown
title: Disclaimer
layout: markdown_table
---


{: .mt-4}
Die Deutsche Zentralbibliothek für Medizin (ZB MED) – Informationszentrum Lebenswissenschaften ist als Inhaltsanbieter nach § 7 Abs. 1 Telemediengesetz für die „eigenen Inhalte“, die auf www.nfdi4microbiota.de, www.nfdi4health.de, www.zbmed.de, www.livivo.de, www.publisso.de, http://books.publisso.de, https://repository.publisso.de und zbmedblog.de zur Nutzung bereitgehalten werden, nach den allgemeinen Gesetzen verantwortlich. Von diesen eigenen Inhalten sind Querverweise („Links“) auf die von anderen Anbietern bereitgehaltenen Inhalte zu unterscheiden. Durch den Querverweis hält die Deutsche Zentralbibliothek für Medizin (ZB MED) – Informationszentrum Lebenswissenschaften insofern „fremde Inhalte“ zur Nutzung bereit, die durch den Hinweis „[extern]“ entsprechend gekennzeichnet sind. Diese fremden Inhalte wurden bei der erstmaligen Link-Setzung daraufhin überprüft, ob durch sie eine mögliche zivilrechtliche oder strafrechtliche Verantwortlichkeit ausgelöst wird. Es ist jedoch nicht auszuschließen, dass die Inhalte im Nachhinein von den jeweiligen Anbietern verändert werden. Die Deutsche Zentralbibliothek für Medizin (ZB MED) – Informationszentrum Lebenswissenschaften überprüft die Inhalte, auf die sie in ihrem Angebot verweist, nicht ständig auf Veränderungen, die eine Verantwortlichkeit neu begründen könnten. Sollten Sie der Ansicht sein, dass die verlinkten externen Seiten gegen geltendes Recht verstoßen oder sonst unangemessene Inhalte haben, so teilen Sie uns dies bitte mit.
{: style="margin-bottom:20rem"}


