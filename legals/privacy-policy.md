---
custom_css: markdown
title: Privacy policy
layout: markdown_table
---




§ 1 <br/>
{: .mt-4}
Information über die Datenerhebung, Verantwortlicher, Kontaktaufnahme

(1) Im Folgenden informieren wir über die Erhebung personenbezogener Daten bei Nutzung unserer Websites www.nfdi4microbiota.de, www.nfdi4life.de, www.zbmed.de, www.livivo.de, www.publisso.de, https://books.publisso.de, https://repository.publisso.de und zbmedblog.de. Personenbezogene Daten sind alle Daten, die auf Sie persönlich beziehbar sind, z. B. Name, Adresse, E-Mail-Adressen, Nutzerverhalten.

(2) Verantwortlicher gem. Art. 4 Abs. 7 EU-Datenschutz-Grundverordnung (DS-GVO) ist:

Deutsche Zentralbibliothek für Medizin (ZB MED) – Informationszentrum Lebenswissenschaften<br/>
Gleueler Straße 60<br/>
50931 Köln<br/>
Tel.: +49 (0)221 478-5686 (Infocenter)<br/>
[E-Mail](mailto:info@zbmed.de)

<b>Vertretungsberechtigte Personen:</b> <br/>
Kaufmännische Geschäftsführerin: [Gabriele Herrmann-Krotz](mailto:herrmann-krotz@zbmed.de), Diplom-Volkswirtin<br/>
Wissenschaftliche Leitung: [Prof. Dr. Dietrich Rebholz-Schuhmann](https://www.nfdi4health.de/privacy-policy/rebholz@zbmed.de)

Unseren Datenschutzbeauftragten erreichen Sie unter:
Datenschutz@zbmed.de
oder unserer Postadresse mit dem Zusatz „der Datenschutzbeauftragte“.

(3) Bei Ihrer Kontaktaufnahme mit uns per E-Mail oder über unsere Kontaktformulare werden die von Ihnen mitgeteilten Daten (Ihre E-Mail-Adresse, ggf. Ihr Name und Ihre Telefonnummer) von uns gespeichert, um Ihre Fragen zu beantworten. Die in diesem Zusammenhang anfallenden Daten löschen wir, nachdem die Speicherung nicht mehr erforderlich ist, oder schränken die Verarbeitung ein, falls gesetzliche Aufbewahrungspflichten bestehen.

(4) Falls wir für einzelne Funktionen unseres Angebots auf beauftragte Dienstleister zurückgreifen oder Ihre Daten für werbliche Zwecke nutzen möchten, werden wir Sie untenstehend im Detail über die jeweiligen Vorgänge informieren. Dabei nennen wir auch die festgelegten Kriterien der Speicherdauer.
{: .mb-4}

§ 2 <br/>
Ihre Rechte

(1) Sie haben gegenüber uns folgende Rechte hinsichtlich der Sie betreffenden personenbezogenen Daten:
- Recht auf Auskunft,
- Recht auf Berichtigung oder Löschung,
- Recht auf Einschränkung der Verarbeitung,
- Recht auf Widerspruch gegen die Verarbeitung,
- Recht auf Datenübertragbarkeit.

(2) Sie haben zudem das Recht, sich bei der zuständigen Datenschutz-Aufsichtsbehörde, der

Landesbeauftragten für Datenschutz und Informationsfreiheit Nordrhein-Westfalen<br/>
Kavalleriestr. 2-4<br/>
40213 Düsseldorf<br/>
Telefon: 0211/38424-0<br/>
Fax: 0211/38424-10<br/>
[E-Mail](mailto:poststelle@ldi.nrw.de)<br/>
[Website](https://www.ldi.nrw.de/index.php)

über die Verarbeitung Ihrer personenbezogenen Daten durch uns zu beschweren.
{: .mb-4}

§ 3 <br/>
Welche Daten wir bei Ihrem Besuch auf unserer Website erheben

(1) Bei der bloß informatorischen Nutzung der Website, also wenn Sie sich nicht registrieren oder uns anderweitig Informationen übermitteln, erheben wir nur die personenbezogenen Daten, die Ihr Browser an unseren Server übermittelt. Wenn Sie unsere Website betrachten möchten, erheben wir die folgenden Daten, die für uns technisch erforderlich sind, um Ihnen unsere Website anzuzeigen und die Stabilität und Sicherheit zu gewährleisten (Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO):
– IP-Adresse
– Datum und Uhrzeit der Anfrage
– Zeitzonendifferenz zur Greenwich Mean Time (GMT)
– Inhalt der Anforderung (konkrete Seite)
– Zugriffsstatus/HTTP-Statuscode
– jeweils übertragene Datenmenge
– Website, von der die Anforderung kommt
– Browser
– Betriebssystem und dessen Oberfläche
– Sprache und Version der Browsersoftware.

(2) Zusätzlich zu den zuvor genannten Daten werden bei Ihrer Nutzung unserer Website Cookies auf Ihrem Rechner gespeichert. Bei Cookies handelt es sich um kleine Textdateien, die lokal im Zwischenspeicher des Internet-Browsers des Seitenbesuchers gespeichert werden. Die Cookies ermöglichen die Wiedererkennung des Internet-Browsers und lassen der Stelle, die den Cookie setzt (hier uns), bestimmte Informationen zufließen. Cookies können keine Programme ausführen oder Viren auf Ihren Computer übertragen. Sie dienen dazu, das Internetangebot insgesamt nutzerfreundlicher und effektiver zu machen.

(3) Einsatz von Cookies:
a) Für die unter § 1 (1) genannten Websites nutzen wir folgende Arten von Cookies, deren Umfang und Funktionsweise im Folgenden erläutert werden:
– Transiente Cookies (dazu b)
– Persistente Cookies (dazu c).
b) Transiente Cookies werden automatisiert gelöscht, wenn Sie den Browser schließen. Dazu zählen insbesondere die Session-Cookies. Diese speichern eine sogenannte Session-ID, mit welcher sich verschiedene Anfragen Ihres Browsers der gemeinsamen Sitzung zuordnen lassen. Dadurch kann Ihr Rechner wiedererkannt werden, wenn Sie auf unsere Website zurückkehren. Die Session-Cookies werden gelöscht, wenn Sie sich ausloggen oder den Browser schließen.
c) Persistente Cookies werden automatisiert nach einer vorgegebenen Dauer gelöscht, die sich je nach Cookie unterscheiden kann. Sie können die Cookies in den Sicherheitseinstellungen Ihres Browsers jederzeit löschen.
d) Sie können Ihre Browser-Einstellung entsprechend Ihren Wünschen konfigurieren und z. B. die Annahme von Third-Party-Cookies oder allen Cookies ablehnen. Wir weisen Sie darauf hin, dass Sie dann eventuell nicht alle Funktionen dieser Website nutzen können.
e) Falls Sie über einen Account bei www.livivo.de, https://books.publisso.de oder https://repository.publisso.de verfügen, setzen wir Cookies ein, um Sie für Folgebesuche identifizieren zu können. Andernfalls müssten Sie sich für jeden Besuch erneut einloggen.
{: .mb-4}

§ 4 <br/>
Welche weiteren Daten wir bei der Nutzung der weiteren Funktionen und Angebote auf unserer Website erheben und was wir damit machen

(1) Neben der rein informatorischen Nutzung unserer Website bieten wir verschiedene Leistungen an, die Sie bei Interesse nutzen können. Dazu gehört die Nutzung von www.livivo.de, https://books.publisso.de, https://repository.publisso.de sowie eines bei uns registrierten Bibliotheksbenutzerkontos. Dazu müssen Sie in der Regel weitere personenbezogene Daten angeben, die wir zur Erbringung der jeweiligen Leistung nutzen und für die die zuvor genannten Grundsätze zur Datenverarbeitung gelten.

(2) Teilweise bedienen wir uns zur Verarbeitung Ihrer Daten externer Dienstleister. Diese wurden von uns sorgfältig ausgewählt und beauftragt, sind an unsere Weisungen gebunden und werden regelmäßig kontrolliert.

(3) Weiterhin können wir Ihre personenbezogenen Daten an Dritte weitergeben, wenn Aktionsteilnahmen, Vertragsabschlüsse oder ähnliche Leistungen von uns gemeinsam mit Partnern angeboten werden. Nähere Informationen hierzu erhalten Sie bei Angabe Ihrer personenbezogenen Daten oder untenstehend in der Beschreibung des Angebo-tes.

(4) Soweit unsere Dienstleister oder Partner ihren Sitz in einem Staat außerhalb des Europäischen Wirtschaftsraumes (EWR) haben, informieren wir Sie über die Folgen dieses Umstands in der Beschreibung des Angebotes.
{: .mb-4}

§ 5 <br/>
Widerspruch gegen die/Widerruf der Verarbeitung Ihrer Daten

(1) Falls Sie eine Einwilligung zur Verarbeitung Ihrer Daten erteilt haben, können Sie diese jederzeit widerrufen. Ein solcher Widerruf beeinflusst die Zulässigkeit der Verarbeitung Ihrer personenbezogenen Daten, nachdem Sie ihn gegenüber uns ausgesprochen haben.

(2) Soweit wir die Verarbeitung Ihrer personenbezogenen Daten auf die Interessenabwägung stützen, können Sie Widerspruch gegen die Verarbeitung einlegen. Dies ist der Fall, wenn die Verarbeitung insbesondere nicht zur Erfüllung eines Vertrags mit Ihnen erforderlich ist, was von uns jeweils bei der nachfolgenden Beschreibung der Funktionen dargestellt wird. Bei Ausübung eines solchen Widerspruchs bitten wir um Darlegung der Gründe, weshalb wir Ihre personenbezogenen Daten nicht wie von uns durchgeführt verarbeiten sollten. Im Falle Ihres begründeten Widerspruchs prüfen wir die Sachlage und werden entweder die Datenverarbeitung einstellen bzw. anpassen oder Ihnen unsere zwingenden schutzwürdigen Gründe aufzeigen, aufgrund derer wir die Verarbeitung fortführen.

(3) Selbstverständlich können Sie der Verarbeitung Ihrer personenbezogenen Daten für Zwecke der Datenanalyse jederzeit widersprechen.
{: .mb-4}

§ 6 <br/>
Nutzung unserer Blog-Funktion, Nutzung unseres Webshops, Nutzung unserer Portale

1. Nutzung der Blog-Funktionen
In unserem Blog (zbmedblog.de), in dem wir verschiedene Beiträge zu Themen rund um unsere Tätigkeiten veröffentlichen, können Sie öffentliche Kommentare abgeben. Ihr Kommentar wird mit Ihrem angegebenen Nutzernamen bei dem Beitrag veröffentlicht. Wir empfehlen, ein Pseudonym statt Ihres Klarnamens zu verwenden. Die Angabe von Nutzernamen und E-Mail-Adresse ist erforderlich, alle weiteren Informationen sind freiwillig. Wenn Sie einen Kommentar abgeben, speichern wir weiterhin Ihre IP-Adresse. Die Speicherung ist für uns erforderlich, um uns in Fällen einer möglichen Veröffentlichung widerrechtlicher Inhalte gegen Haftungsansprüche verteidigen zu können. Ihre E-Mail-Adresse benötigen wir, um mit Ihnen in Kontakt zu treten, falls ein Dritter Ihren Kommentar als rechtswidrig beanstanden sollte. Rechtsgrundlagen sind Art. 6 Abs. 1 S. 1 lit. b und f DS-GVO. Die Kommentare werden vor Veröffentlichung geprüft. Wir behalten uns vor, Kommentare zu löschen, wenn sie von Dritten als rechtswidrig beanstandet werden.

2. Nutzung unseres Webshops
(1) Wenn Sie in unserem Webshop auf www.livivo.de bestellen möchten, ist es für den Vertragsabschluss erforderlich, dass Sie Ihre persönlichen Daten angeben, die wir für die Abwicklung Ihrer Bestellung benötigen. Für die Abwicklung der Verträge notwendige Pflichtangaben sind gesondert markiert, weitere Angaben sind freiwillig. Die von Ihnen angegebenen Daten verarbeiten wir zur Abwicklung Ihrer Bestellung. Dazu können wir Ihre Zahlungsdaten an unsere Hausbank weitergeben. Rechtsgrundlage hierfür ist Art. 6 Abs. 1 S. 1 lit. b DS-GVO.

Wir können die von Ihnen angegebenen Daten zudem verarbeiten, um Sie über weitere interessante Produkte aus unserem Portfolio zu informieren oder Ihnen E-Mails mit technischen Informationen zukommen lassen.
(2) Wir sind aufgrund handels- und steuerrechtlicher Vorgaben verpflichtet, Ihre Adress-, Zahlungs- und Bestelldaten für die Dauer von zehn Jahren zu speichern. Allerdings nehmen wir nach zwei Jahren eine Einschränkung der Verarbeitung vor, d. h. Ihre Daten werden nur zur Einhaltung der gesetzlichen Verpflichtungen eingesetzt.
(3) Zur Verhinderung unberechtigter Zugriffe Dritter auf Ihre persönlichen Daten, insbesondere Finanzdaten, wird der Bestellvorgang per TLS-Technik verschlüsselt.

3. Nutzung unserer Portale www.livivo.de, www.publisso.de, https://books.publisso.de, https://repository.publisso.de
(1) Soweit Sie unsere Portale www.publisso.de, https://books.publisso.de und https://repository.publisso.de als Autor, Herausgeber oder Reviewer nutzen möchten, müssen Sie sich mittels Angabe Ihrer E-Mail-Adresse, eines selbst gewählten Passworts sowie Ihres frei wählbaren Benutzernamens registrieren. Damit nachvollzogen werden kann ob und wie Sie informiert wurden, erheben wir im Rahmen der E-Mail Korrespondenz zudem folgende Daten: Empfänger, Absender, Zeitstempel, Betreff. Auf unserem Portal https://books.publisso.de besteht Klarnamenszwang, eine pseudonyme Nutzung ist nicht möglich.

Soweit Sie www.livivo.de nutzen möchten, können Sie sich mittels Angabe Ihrer E-Mail-Adresse, Name und Adresse sowie eines selbst gewählten Passworts registrieren. Ohne Anmeldung stehen nicht alle Funktionen zur Verfügung.

Wir verwenden für die Registrierung auf unseren Portalen das sog. Double-opt-in-Verfahren, d. h. Ihre Registrierung ist erst abgeschlossen, wenn Sie zuvor Ihre Anmeldung über eine Ihnen zu diesem Zweck zugesandte Bestätigungs-E-Mail durch Klick auf den darin enthaltenem Link bestätigt haben. Falls Ihre diesbezügliche Bestätigung nicht binnen 24 Stunden erfolgt, wird Ihre Anmeldung automatisch aus unserer Datenbank gelöscht.

Die Angabe der zuvor genannten Daten ist verpflichtend, alle weiteren Informationen können Sie freiwillig durch Nutzung unseres Portals bereitstellen.
(2) Wenn Sie unsere Portale nutzen, speichern wir Ihre zur Vertragserfüllung erforderlichen Daten, für www.livivo.de auch Angaben zur Zahlungsweise, bis Sie Ihren Zugang endgültig löschen. Weiterhin speichern wir die von Ihnen angegebenen freiwilligen Daten für die Zeit Ihrer Nutzung des Portals, soweit Sie diese nicht zuvor löschen. Alle Angaben können Sie im geschützten Kundenbereich verwalten und ändern. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO.
(3) Wenn Sie unsere Portale https://books.publisso.de und https://repository.publisso.de nutzen, können Ihre Daten entsprechend der Vertragsleistung anderen Teilnehmern des Portals zugänglich werden.
Auf unserem Portal https://books.publisso.de sind alle angegebenen Daten sichtbar. Sie haben die Möglichkeit zu entscheiden, ob Ihre Daten ausgegeben werden.
Auf https://repository.publisso.de ist Ihr Nickname sichtbar. Unsere Moderatoren sehen darüber hinaus die von Ihnen angegebene E-Mailadresse.
Nicht angemeldete Mitglieder erhalten keine Informationen über Sie. Für alle angemeldeten Mitglieder sind Ihr Benutzername und Foto sichtbar, unabhängig davon, ob Sie diese freigegeben haben. Demgegenüber ist ihr gesamtes Profil mit den von Ihnen freigegebenen Daten für alle Mitglieder sichtbar, die Sie als persönlichen Kontakt bestätigt haben. Wenn Sie Ihren persönlichen Kontakten Inhalte zugänglich machen, die Sie nicht mittels einer privaten Nachricht senden, sind diese Inhalte für Dritte einsehbar, soweit Ihr persönlicher Kontakt die Freigabe erteilt hat. Soweit Sie Beiträge in öffentlichen Gruppen einstellen, sind diese für alle angemeldeten Mitglieder des Portals sichtbar.
(4) Um unberechtigte Zugriffe Dritter auf Ihre persönlichen Daten, insbesondere Finanzdaten, zu verhindern, wird die Verbindung per TLS-Technik verschlüsselt.
{: .mb-4}

§ 7 <br/>
Unser Newsletter

(1) Mit Ihrer Einwilligung können Sie unseren Newsletter abonnieren, mit dem wir Sie über unsere aktuellen interessanten Angebote informieren. Die beworbenen Waren und Dienstleistungen sind in der Einwilligungserklärung benannt.
(2) Für die Anmeldung zu unserem Newsletter verwenden wir das sog. Double-opt-in-Verfahren. Das heißt, dass wir Ihnen nach Ihrer Anmeldung eine E-Mail an die angegebene E-Mail-Adresse senden, in welcher wir Sie um Bestätigung bitten, dass Sie den Versand des Newsletters wünschen. Wenn Sie Ihre Anmeldung nicht innerhalb von 24 Stunden bestätigen, werden Ihre Informationen gesperrt und nach einem Monat automatisch gelöscht. Darüber hinaus speichern wir jeweils Ihre eingesetzten IP-Adressen und Zeitpunkte der Anmeldung und Bestätigung. Zweck des Verfahrens ist, Ihre Anmeldung nachweisen und ggf. einen möglichen Missbrauch Ihrer persönlichen Daten aufklären zu können.
(3) Pflichtangabe für die Übersendung des Newsletters ist allein Ihre E-Mail-Adresse. Die Angabe weiterer, gesondert markierter Daten ist freiwillig und wird verwendet, um Sie persönlich ansprechen zu können. Nach Ihrer Bestätigung speichern wir Ihre E-Mail-Adresse zum Zweck der Zusendung des Newsletters. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. a DS-GVO.
(4) Ihre Einwilligung in die Übersendung des Newsletters können Sie jederzeit widerrufen und den Newsletter abbestellen. Den Widerruf können Sie durch Klick auf den in jeder Newsletter-E-Mail bereitgestellten Link, für unseren ZB MED-Newsletter per E-Mail an webredaktion@zbmed.de oder durch eine Nachricht an die im Impressum angegebenen Kontaktdaten erklären.
{: .mb-4}

§ 8 <br/>
Einsatz von Web Analytics

1. Einsatz von Google Analytics
(1) Unserer Websites www.zbmed.de, www.livivo.de, www.publisso.de benutzen Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Website, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Website-Aktivitäten zusammenzustellen und um weitere mit der Website-Nutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Website-Betreiber zu erbringen.
(2) Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.
(3) Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plug-in herunterladen und installieren: http://tools.google.com/dlpage/gaoptout?hl=de.
(4) Die in Absatz (1) benannten Websites verwenden Google Analytics mit der Erweiterung "anonymizeIp()“. Dadurch werden IP-Adressen gekürzt weiterverarbeitet, eine Personenbeziehbarkeit kann damit ausgeschlossen werden. Soweit den über Sie er-hobenen Daten ein Personenbezug zukommt, wird dieser also sofort ausgeschlossen und die personenbezogenen Daten damit umgehend gelöscht.
(5) Wir nutzen Google Analytics, um die Nutzung unserer Website analysieren und regelmäßig verbessern zu können. Über die gewonnenen Statistiken können wir unser Angebot verbessern und für Sie als Nutzer interessanter ausgestalten. Für die Ausnahmefälle, in denen personenbezogene Daten in die USA übertragen werden, hat sich Google dem EU-US Privacy Shield unterworfen, https://www.privacyshield.gov/EU-US-Framework. Rechtsgrundlage für die Nutzung von Google Analytics ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO.
(6) Informationen des Drittanbieters: Google Dublin, Google Ireland Ltd., Gordon House, Barrow Street, Dublin 4, Ireland, Fax: +353 (1) 436 1001. Nutzerbedingungen: http://www.google.com/analytics/terms/de.html, Übersicht zum Datenschutz: http://www.google.com/intl/de/analytics/learn/privacy.html, sowie die Datenschutzerklärung: http://www.google.de/intl/de/policies/privacy.

2. Einsatz von Piwik/Matomo
(1) Unsere Websites https://books.publisso.de und https://repository.publisso.de nutzten den Webanalysedienst Piwik/Matomo, um die Nutzung unserer Website analysieren und regelmäßig verbessern zu können. Über die gewonnenen Statistiken können wir unser Angebot verbessern und für Sie als Nutzer interessanter ausgestalten. Rechtsgrundlage für die Nutzung von Piwik/Matomo ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO.
(2) Für diese Auswertung werden Cookies (näheres dazu in § 3) auf Ihrem Computer gespeichert. Die so erhobenen Informationen speichert der Verantwortliche ausschließlich auf seinem Server in Deutschland. Die Auswertung können Sie einstellen durch Löschung vorhandener Cookies und die Verhinderung der Speicherung von Cookies. Wenn Sie die Speicherung der Cookies verhindern, weisen wir darauf hin, dass Sie gegebenenfalls diese Website nicht vollumfänglich nutzen können. Die Verhinderung der Speicherung von Cookies ist durch die Einstellung in ihrem Browser möglich. Die Verhinderung des Einsatzes von Piwik/Matomo ist möglich, indem Sie den folgenden Haken entfernen und so das Opt-out-Plug-in aktivieren: [Piwik/Matomo iFrame].
(3) Diese Website verwendet Piwik/Matomo mit der Erweiterung „AnonymizeIP“. Dadurch werden IP-Adressen gekürzt weiterverarbeitet, eine direkte Personenbeziehbarkeit kann damit ausgeschlossen werden. Die mittels Piwik/Matomo von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen von uns erhobenen Daten zusammengeführt.
(4) Das Programm Piwik/Matomo ist ein Open-Source-Projekt. Informationen des Drittanbieters zum Datenschutz erhalten Sie unter http://piwik.org/privacy/policy.

3. Einsatz von Jetpack/ehem. WordPress.com-Stats
(1) zbmedblog.de nutzt den Webanalysedienst Jetpack (früher: WordPress.com-Stats), um die Nutzung unserer Website analysieren und regelmäßig verbessern zu können. Über die gewonnenen Statistiken können wir unser Angebot verbessern und für Sie als Nutzer interessanter ausgestalten. Weiterhin nutzen wir das System für Maßnahmen zum Schutz der Sicherheit der Website, z. B. dem Erkennen von Angriffen oder Viren. Für die Ausnahmefälle, in denen personenbezogene Daten in die USA übertragen werden, hat sich Automattic Inc. dem EU-US Privacy Shield unterworfen, https://www.privacyshield.gov. Rechtsgrundlage für die Nutzung von Jetpack ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO.
(2) Für diese Auswertung werden Cookies (näheres dazu in § 3) auf Ihrem Computer gespeichert. Die so erhobenen Informationen werden auf einem Server in den USA gespeichert. Wenn Sie die Speicherung der Cookies verhindern, weisen wir darauf hin, dass Sie gegebenenfalls diese Website nicht vollumfänglich nutzen können. Die Verhinderung der Speicherung von Cookies ist möglich durch die Einstellung in ihrem Browser oder indem Sie den Button „Click here to Opt-out“ unter http://www.quantcast.com/opt-out betätigen.
(3) Diese Website verwendet Jetpack mit einer Erweiterung, durch die IP-Adressen direkt nach ihrer Erhebung gekürzt weiterverarbeitet werden, um so eine Personenbeziehbarkeit auszuschließen.
(4) Informationen des Drittanbieters: Automattic Inc., 60 29 th Street #343, San Francisco, CA 94110–4929, USA, https://automattic.com/privacy, sowie des Drittanbieters der Trackingtechnologie: Quantcast Inc., 201 3 rd St, Floor 2, San Francisco, CA 94103–3153, USA, https://www.quantcast.com/privacy.
{: .mb-4}

§ 9 <br/>
Social Media

1. Einsatz von Social-Media-Buttons
(1) Wir setzen derzeit über “c’t Shariff” folgende Social-Media-Buttons ein: Youtube, Flickr, Google+, Twitter, Xing, LinkedIn.
Das c’t-Projekt Shariff ersetzt die üblichen Share-Buttons der Social Networks und schützt Ihr Surf-Verhalten vor neugierigen Blicken. Dennoch reicht ein einziger Klick auf den Button, um Informationen mit anderen zu teilen.
Die üblichen Social-Media-Buttons übertragen die User-Daten bei jedem Seitenaufruf an Facebook & Co. und geben den sozialen Netzwerken genaue Auskunft über Ihr Surfverhalten (User Tracking). Dazu müssen Sie weder eingeloggt noch Mitglied des Netzwerks sein. Dagegen stellt ein Shariff-Button den direkten Kontakt zwischen Social Network und Besucher erst dann her, wenn letzterer aktiv auf den Share-Button klickt. Damit verhindert Shariff, dass Sie auf jeder besuchten Seite eine digitale Spur hinterlassen. Die Anzeige der “Likes”, „+1en“ oder „tweets“ kommt dank Shariff vom Betreiber der Seite mit den Knöpfen. Shariff tritt hier als Zwischeninstanz auf: An Stelle des Browsers fragt der Server des Webseiten-Betreibers die Zahl der “Likes”, „+1en“ oder „tweets“ ab – und dies auch nur einmal pro Minute, um den Traffic in Grenzen zu halten. Der Besucher bleibt hierbei anonym.
Weitere Informationen zu “c’t Shariff” finden Sie unter: https://www.heise.de/ct/artikel/Shariff-Social-Media-Buttons-mit-Datenschutz-2467514.html sowie unter: https://www.heise.de/ct/ausgabe/2014-26-Social-Media-Buttons-datenschutzkonform-nutzen-2463330.html.
(2) Adresse des jeweiligen Button-Anbieters und URL mit dessen Datenschutzhinweisen:
Heise Medien GmbH & Co. KG, Karl-Wiechert-Allee 10, 30625 Hannover; https://www.heise.de/Privacy-Policy-der-Heise-Medien-GmbH-Co-KG-4860.html.

2. Einbindung von YouTube-Videos
(1) Wir haben YouTube-Videos in unser Online-Angebot eingebunden, die auf http://www.YouTube.com gespeichert sind und von unserer Website aus direkt abspielbar sind. Diese sind alle im „erweiterten Datenschutz-Modus“ eingebunden, d. h. dass keine Daten über Sie als Nutzer an YouTube übertragen werden, wenn Sie die Videos nicht abspielen. Erst wenn Sie die Videos abspielen, werden die in Absatz 2 genannten Daten übertragen. Auf diese Datenübertragung haben wir keinen Einfluss.
(2) Durch den Besuch auf der Website erhält YouTube die Information, dass Sie die ent-sprechende Unterseite unserer Website aufgerufen haben. Zudem werden die unter § 3 dieser Erklärung genannten Daten übermittelt. Dies erfolgt unabhängig davon, ob YouTube ein Nutzerkonto bereitstellt, über das Sie eingeloggt sind, oder ob kein Nutzerkonto besteht. Wenn Sie bei Google eingeloggt sind, werden Ihre Daten direkt Ihrem Konto zugeordnet. Wenn Sie die Zuordnung mit Ihrem Profil bei YouTube nicht wünschen, müssen Sie sich vor Aktivierung des Buttons ausloggen. YouTube speichert Ihre Daten als Nutzungsprofile und nutzt sie für Zwecke der Werbung, Marktfor-schung und/oder bedarfsgerechten Gestaltung seiner Website. Eine solche Auswertung erfolgt insbesondere (selbst für nicht eingeloggte Nutzer) zur Erbringung von be-darfsgerechter Werbung und um andere Nutzer des sozialen Netzwerks über Ihre Aktivitäten auf unserer Website zu informieren. Ihnen steht ein Widerspruchsrecht zu gegen die Bildung dieser Nutzerprofile, wobei Sie sich zur Ausübung dessen an YouTube richten müssen.
(3) Weitere Informationen zu Zweck und Umfang der Datenerhebung und ihrer Verarbeitung durch YouTube erhalten Sie in der Datenschutzerklärung von YouTube. Dort erhalten Sie auch weitere Informationen zu Ihren Rechten und Einstellungsmöglichkeiten zum Schutz Ihrer Privatsphäre: https://www.google.de/intl/de/policies/privacy. Google verarbeitet Ihre personenbezogenen Daten auch in den USA und hat sich dem EU-US-Privacy-Shield unterworfen, https://www.privacyshield.gov/EU-US-Framework.

3. Einbindung von Google Maps
(1) Auf unserer Webseite www.zbmed.de nutzen wir das Angebot von Google Maps. Dadurch können wir Ihnen interaktive Karten direkt in der Website anzeigen und ermöglichen Ihnen die komfortable Nutzung der Karten-Funktion.
(2) Durch den Besuch auf der Website erhält Google die Information, dass Sie die ent-sprechende Unterseite unserer Website aufgerufen haben. Zudem werden die unter § 3 dieser Erklärung genannten Daten übermittelt. Dies erfolgt unabhängig davon, ob Google ein Nutzerkonto bereitstellt, über das Sie eingeloggt sind, oder ob kein Nutzer-konto besteht. Wenn Sie bei Google eingeloggt sind, werden Ihre Daten direkt Ihrem Konto zugeordnet. Wenn Sie die Zuordnung mit Ihrem Profil bei Google nicht wün-schen, müssen Sie sich vor Aktivierung des Buttons ausloggen. Google speichert Ihre Daten als Nutzungsprofile und nutzt sie für Zwecke der Werbung, Marktforschung und/oder bedarfsgerechten Gestaltung seiner Website. Eine solche Auswertung erfolgt insbesondere (selbst für nicht eingeloggte Nutzer) zur Erbringung von bedarfsgerech-ter Werbung und um andere Nutzer des sozialen Netzwerks über Ihre Aktivitäten auf unserer Website zu informieren. Ihnen steht ein Widerspruchsrecht zu gegen die Bil-dung dieser Nutzerprofile, wobei Sie sich zur Ausübung dessen an Google richten müssen.
(3) Weitere Informationen zu Zweck und Umfang der Datenerhebung und ihrer Verarbei-tung durch den Plug-in-Anbieter erhalten Sie in den Datenschutzerklärungen des An-bieters. Dort erhalten Sie auch weitere Informationen zu Ihren diesbezüglichen Rech-ten und Einstellungsmöglichkeiten zum Schutze Ihrer Privatsphäre: http://www.google.de/intl/de/policies/privacy. Google verarbeitet Ihre personenbezo-genen Daten auch in den USA und hat sich dem EU-US Privacy Shield unterworfen, https://www.privacyshield.gov/EU-US-Framework.
{: .mb-4}

§ 10 <br/>
Online-Werbung

1. Einsatz von Google Adwords Conversion6
(1) Auf www.zbmed.de, www.publisso.de und www.livivo.de nutzen wir das Angebot von Google Adwords, um mit Hilfe von Werbemitteln (sogenannten Google Adwords) auf externen Webseiten auf unsere Angebote aufmerksam zu machen. Wir können in Re-lation zu den Daten der Werbekampagnen ermitteln, wie erfolgreich die einzelnen Werbemaßnahmen sind. Wir verfolgen damit das Interesse, Ihnen Werbung anzuzeigen, die für Sie von Interesse ist, unsere Website für Sie interessanter zu gestalten und eine faire Berechnung von Werbe-Kosten zu erreichen.
(2) Diese Werbemittel werden durch Google über sogenannte „Ad Server“ ausgeliefert. Dazu nutzen wir Ad Server Cookies, durch die bestimmte Parameter zur Erfolgsmes-sung, wie Einblendung der Anzeigen oder Klicks durch die Nutzer, gemessen werden können. Sofern Sie über eine Google-Anzeige auf unsere Website gelangen, wird von Google Adwords ein Cookie in ihrem PC gespeichert. Dieser Cookie verliert in der Regel nach 30 Tagen seine Gültigkeit und soll nicht dazu dienen, Sie persönlich zu identifizieren. Zu diesem Cookie werden in der Regel als Analyse-Werte die Unique Cookie-ID, Anzahl Ad Impressions pro Platzierung (Frequency), letzte Impression (re-levant für Post-View-Conversions) sowie Opt-out-Informationen (Markierung, dass der Nutzer nicht mehr angesprochen werden möchte) gespeichert.
(3) Diese Cookies ermöglichen Google, Ihren Internetbrowser wiederzuerkennen. Sofern ein Nutzer bestimmte Seiten der Webseite eines Adwords-Kunden besucht und das auf seinem Computer gespeicherte Cookie noch nicht abgelaufen ist, können Google und der Kunde erkennen, dass der Nutzer auf die Anzeige geklickt hat und zu dieser Seite weitergeleitet wurde. Jedem Adwords-Kunden wird ein anderes Cookie zuge-ordnet. Cookies können somit nicht über die Webseiten von Adwords-Kunden nach-verfolgt werden. Wir selbst erheben und verarbeiten in den genannten Werbemaß-nahmen keine personenbezogenen Daten. Wir erhalten von Google lediglich statisti-sche Auswertungen zur Verfügung gestellt. Anhand dieser Auswertungen können wir erkennen, welche der eingesetzten Werbemaßnahmen besonders effektiv sind. Wei-tergehende Daten aus dem Einsatz der Werbemittel erhalten wir nicht, insbesondere können wir die Nutzer nicht anhand dieser Informationen identifizieren.
(4) Aufgrund der eingesetzten Marketing-Tools baut Ihr Browser automatisch eine direkte Verbindung mit dem Server von Google auf. Wir haben keinen Einfluss auf den Um-fang und die weitere Verwendung der Daten, die durch den Einsatz dieses Tools durch Google erhoben werden und informieren Sie daher entsprechend unserem Kenntnis-stand: Durch die Einbindung von AdWords Conversion erhält Google die Information, dass Sie den entsprechenden Teil unseres Internetauftritts aufgerufen oder eine An-zeige von uns angeklickt haben. Sofern Sie bei einem Dienst von Google registriert sind, kann Google den Besuch Ihrem Account zuordnen. Selbst wenn Sie nicht bei Google registriert sind bzw. sich nicht eingeloggt haben, besteht die Möglichkeit, dass der Anbieter Ihre IP-Adresse in Erfahrung bringt und speichert.
(5) Sie können die Teilnahme an diesem Tracking-Verfahren auf verschiedene Weise verhindern:
a) durch eine entsprechende Einstellung Ihrer Browser-Software, insbesondere führt die Unterdrückung von Drittcookies dazu, dass Sie keine Anzeigen von Drittanbietern erhalten;
b) durch Deaktivierung der Cookies für Conversion-Tracking, indem Sie Ihren Brow-ser so einstellen, dass Cookies von der Domain www.googleadservices.com blockiert werden, https://www.google.de/settings/ads, wobei diese Einstellung ge-löscht werden, wenn Sie Ihre Cookies löschen;
c) durch Deaktivierung der interessenbezogenen Anzeigen der Anbieter, die Teil der Selbstregulierungs-Kampagne „About Ads“ sind, über den Link http://www.aboutads.info/choices, wobei diese Einstellung gelöscht wird, wenn Sie Ihre Cookies löschen;
d) durch dauerhafte Deaktivierung in Ihren Browsern Firefox, Internetexplorer oder Google Chrome unter dem Link http://www.google.com/settings/ads/plugin.
Wir weisen Sie darauf hin, dass Sie in diesem Fall gegebenenfalls nicht alle Funktionen dieses Angebots vollumfänglich nutzen können.
(6) Rechtsgrundlage für die Verarbeitung Ihrer Daten ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO. Weitere Informationen zum Datenschutz bei Google finden Sie hier: http://www.google.com/intl/de/policies/privacy und https://services.google.com/sitestats/de.html. Alternativ können Sie die Webseite der Network Advertising Initiative (NAI) unter http://www.networkadvertising.org besu-chen. Google hat sich dem EU-US Privacy Shield unterworfen, https://www.privacyshield.gov/EU-US-Framework.

2. Google Remarketing
Neben Adwords Conversion nutzen wir auf www.zbmed.de, www.publisso.de und www.livivo.de die Anwendung Google Remarketing. Hierbei handelt es sich um ein Verfahren, mit dem wir Sie erneut ansprechen möchten. Durch diese Anwendung können Ihnen nach Besuch unserer Website bei Ihrer weiteren Internetnutzung unsere Werbean-zeigen eingeblendet werden. Dies erfolgt mittels in Ihrem Browser gespeicherter Cookies, über die Ihr Nutzungsverhalten bei Besuch verschiedener Websites durch Google erfasst und ausgewertet wird. So kann von Google Ihr vorheriger Besuch unserer Website fest-gestellt werden. Eine Zusammenführung der im Rahmen des Remarketings erhobenen Daten mit Ihren personenbezogenen Daten, die ggf. von Google gespeichert werden, fin-det durch Google laut eigenen Aussagen nicht statt. Insbesondere wird laut Google beim Remarketing eine Pseudonymisierung eingesetzt.

3. DoubleClick by Google8
(1) Auf www.zbmed.de, www.publisso.de und www.livivo.de nutzen wir weiterhin das Online Marketing Tool DoubleClick by Google. DoubleClick setzt Cookies ein, um für die Nutzer relevante Anzeigen zu schalten, die Berichte zur Kampagnenleistung zu ver-bessern oder um zu vermeiden, dass ein Nutzer die gleichen Anzeigen mehrmals sieht. Über eine Cookie-ID erfasst Google, welche Anzeigen in welchem Browser geschaltet werden und kann so verhindern, dass diese mehrfach angezeigt werden. Darüber hinaus kann DoubleClick mithilfe von Cookie-IDs sog. Conversions erfassen, die Bezug zu Anzeigenanfragen haben. Das ist etwa der Fall, wenn ein Nutzer eine DoubleClick-Anzeige sieht und später mit demselben Browser die Website des Werbe-treibenden aufruft und dort etwas kauft. Laut Google enthalten DoubleClick-Cookies keine personenbezogenen Informationen.
(2) Aufgrund der eingesetzten Marketing-Tools baut Ihr Browser automatisch eine direkte Verbindung mit dem Server von Google auf. Wir haben keinen Einfluss auf den Um-fang und die weitere Verwendung der Daten, die durch den Einsatz dieses Tools durch Google erhoben werden und informieren Sie daher entsprechend unserem Kenntnis-stand: Durch die Einbindung von DoubleClick erhält Google die Information, dass Sie den entsprechenden Teil unseres Internetauftritts aufgerufen oder eine Anzeige von uns angeklickt haben. Sofern Sie bei einem Dienst von Google registriert sind, kann Google den Besuch Ihrem Account zuordnen. Selbst wenn Sie nicht bei Google regis-triert sind bzw. sich nicht eingeloggt haben, besteht die Möglichkeit, dass der Anbieter Ihre IP-Adresse in Erfahrung bringt und speichert.
(3) Sie können die Teilnahme an diesem Tracking-Verfahren auf verschiedene Weise verhindern:
a) durch eine entsprechende Einstellung Ihrer Browser-Software, insbesondere führt die Unterdrückung von Drittcookies dazu, dass Sie keine Anzeigen von Drittanbie-tern erhalten;
b) durch Deaktivierung der Cookies für Conversion-Tracking, indem Sie Ihren Brow-ser so einstellen, dass Cookies von der Domain www.googleadservices.com blockiert werden, https://www.google.de/settings/ads, wobei diese Einstellung ge-löscht wird, wenn Sie Ihre Cookies löschen;
c) durch Deaktivierung der interessenbezogenen Anzeigen der Anbieter, die Teil der Selbstregulierungs-Kampagne „About Ads“ sind, über den Link http://www.aboutads.info/choices, wobei diese Einstellung gelöscht wird, wenn Sie Ihre Cookies löschen;
d) durch dauerhafte Deaktivierung in Ihren Browsern Firefox, Internetexplorer oder Google Chrome unter dem Link http://www.google.com/settings/ads/plugin.
Wir weisen Sie darauf hin, dass Sie in diesem Fall gegebenenfalls nicht alle Funktio-nen dieses Angebots vollumfänglich nutzen können.
(4) Rechtsgrundlage für die Verarbeitung Ihrer Daten ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO. Weitere Informationen zu DoubleClick by Google erhalten Sie unter https://www.google.de/doubleclick und http://support.google.com/adsense/answer/2839090, sowie zum Datenschutz bei Google allgemein: https://www.google.de/intl/de/policies/privacy. Alternativ können Sie die Webseite der Network Advertising Initiative (NAI) unter http://www.networkadvertising.org besuchen. Google hat sich dem EU-US Privacy Shield unterworfen, https://www.privacyshield.gov/EU-US-Framework.

Vorlage für unsere Datenschutzerklärung sind die Formulare aus: Koreng, Ansgar/Lachenmann, Matthias u.a. (Hrsg.), „Formularhandbuch Datenschutzrecht“, C. H. Beck, München, 2. Auflage, 2018. Wir haben diese nach unseren Bedürfnissen angepasst.
{: .mb-4}


