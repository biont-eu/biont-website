---
custom_css: markdown
title: Impressum
layout: markdown_table
---


{: .mt-4}
<b>E-Mail:</b><br/>
[contact@nfdi4microbiota.de](contact@nfdi4microbiota.de)

Deutsche Zentralbibliothek für Medizin (ZB MED) – Informationszentrum Lebenswissenschaften<br/>
Gleueler Straße 60<br/>
50931 Köln<br/>
Tel.: +49 (0)221 478-5685 (Infocenter)<br/>
E-Mail: [info@zbmed.de](info@zbmed.de)<br/>
Stiftung des öffentlichen Rechts

<b>Vertretungsberechtigte Personen:</b><br/>
Kaufmännische Geschäftsführerin: [Gabriele Herrmann-Krotz](mailto:kerrmann-krotz@zbmed.de), Diplom-Volkswirtin<br/>
Wissenschaftliche Leitung: [Prof. Dr. Dietrich Rebholz-Schuhmann](mailto:rebholz@zbmed.de)<br/>

Umsatzsteuer-Identifikationsnummer:<br/>
DE123486783

Zuständige Aufsichtsbehörde:<br/>
Ministerium für Kultur und Wissenschaft – MKW NRW<br/>
des Landes Nordrhein-Westfalen<br/>
Völklinger Straße 49<br/>
40221 Düsseldorf

Tel.: +49 (0)211 896-03 / -04<br/>
Fax: +49 (0)211 896-4555 und -3220<br/>
[Website des MKW NRW](https://www.mkw.nrw/)<br/>
[Kontaktformular des MKW NRW](https://www.mkw.nrw/ministerium/service/kontakt-formular/)

Redaktionell Verantwortlich:<br/>
[Konrad U. Förstner](mailto:foerstner@zbmed.de)

Bildnachweis:<br/>
iStock
{: style="margin-bottom:3rem"}

Flaticon
<a href="https://www.flaticon.com/free-icons/christmas-cap" title="christmas cap icons">Christmas cap icons created by BabyCorn - Flaticon</a>


