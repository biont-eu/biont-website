---
custom_css: markdown
title: Ambassador program
layout: markdown_table
permalink: /ambassador/

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http://biont-training.eu/ambassador
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "Description of the BioNT ambassador program, including the role of ambassadors within consortia"
---


<div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
	<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		<b>The BioNT ambassador program</b>
	</button>
   </h2>
   <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordinExample">
      <div class="accordion-body">
		The vision of BioNT is to provide a high-quality training program and community for digital skills
relevant to the biotechnology industry and biomedical sector. BIONT will structure and consolidate the communication with specific
workplaces by proposing to trainees and trainers the role of “BIONT Ambassadors”.
Ambassadors will receive the project newsletter, and distribute the relevant news to their
institution. The benefit for ambassadors will be to be the first to know about BIONT courses and
initiative (so to have the opportunity to attend multiple courses). In addition, the proponents will
encourage the participation of multiple people from the same institution, to facilitate the blended
format for participation and support the creation of local communities. The number of
ambassadors will be also tracked as an indicator.
connections to numerous institutes from the community have to be established.


 
