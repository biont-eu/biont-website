---
custom_css: markdown
title: Work package 7 - Project management and coordination
layout: markdown_table

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http://biont-training.eu/work-programme/wp7-Project_management_and_coordination.md
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "Description of Work package 7 and relative Tasks"
  "keywords": "WP7, Work package 7"
---
In this WP7 we plan to deliver all the workshops, together with our highly qualified instructors
{: .my-5}

### Objectives


- Project administration and technical coordination

- Measures for inclusivity, accessibility and learning effectiveness








