---
custom_css: markdown
title: Work package 2 - Training needs in industry
layout: markdown_table

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http://biont-training.eu/work-programme/wp2-Training_needs_in_industry.md
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "Description of Work package 2 and relative Tasks"
  "keywords": "WP2, Work package 2"
---
In this WP2 with “industry” we refer to SMEs and other types of businesses including a
research and development department, or a data analysis department, specifically handling
biological-related data. Hence, biotechnological and healthcare-related businesses
{: .my-5}

### Objectives
- Stakeholders analysis

- Analysis of SME training needs

- Analysis of the workshop feedback

- Targeting of project communication to stakeholders
