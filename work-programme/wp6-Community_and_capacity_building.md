---
custom_css: markdown
title: Work package 6 - Community and capacity building 
layout: markdown_table

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http://biont-training.eu/work-programme/wp6-Data_analysis_Information.md
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "Description of Work package 6 and relative Tasks"
  "keywords": "WP6, Work package 6"
---
In this WP6 we take care of all internal and external communications.
{: .my-5}

### Objectives

- Project website and communication tools

- Synergies building with other projects and initiatives

- Translation of materials

- Guidelines for the project sustainability


