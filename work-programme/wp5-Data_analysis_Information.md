---
custom_css: markdown
title: Work package 5 - Data analysis and information
layout: markdown_table

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http://biont-training.eu/work-programme/wp5-Data_analysis_Information
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "Description of Work package 5 and relative Tasks"
  "keywords": "WP5, Work package 5"
---
In this WP5 we take care of all internal and external communications.
{: .my-5}

### Objectives

- Management of the project communication

- Publication of yearly reports and other milestone documents


