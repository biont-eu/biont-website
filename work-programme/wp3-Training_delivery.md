---
custom_css: markdown
title: Work package 3 - Training delivery
layout: markdown_table

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http://biont-training.eu/work-programme/wp3-Training_delivery.md
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "Description of Work package 3 and relative Tasks"
  "keywords": "WP3, Work package 3"
---
In this WP3 we plan to deliver all the workshops, together with our highly qualified instructors
{: .my-5}

### Objectives

- Design and implementation of the delivery format

- Coordination of the courses delivery 





















- Design and implementation of the delivery format
- Coordination of the courses delivery