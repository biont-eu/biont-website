---
custom_css: markdown
title: Work package 4 - Technical Infrastructure
layout: markdown_table

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http://biont-training.eu/work-programme/wp4-Technical_Infrastructure.md
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "Description of Work package 4 and relative Tasks"
  "keywords": "WP4, Work package 4"
---
In this WP4 we plan to deliver all the workshops, together with our highly qualified instructors
{: .my-5}

### Objectives

- Setup and maintenance of platforms for the courses registration and participants selection

- Technical infrastructure to support courses delivery

- Technical infrastructure to support the courses translation

- Editing and maintenance of platform for sharing training materials and recordings

* The maintenance of the platform for the training materials design is not considered in this WP as it will be
adopted from The Carpentries.

