---
custom_css: markdown
title: Work package 1 - Training design and development
layout: markdown_table

schemas:
  "@context": https://schema.org/
  "@type": WebPage
  "@id": http://biont-training.eu/work-programme/wp2-Training_needs_in_industry.md
  "http://purl.org/dc/terms/conformsTo":
    "@type": CreativeWork
    "@id": "https://bioschemas.org/profiles/TrainingMaterial/0.9-DRAFT-2020_12_08/"
  "description": "Description of Work package 2 and relative Tasks"
  "keywords": "WP1, Work package 1"
---
This WP1 is dedicated to the production of  high quality and field specific training materials.
{: .my-5}


### Objectives

- Induction of trainers

- Training materials design and development of 8 workshops, in 2 curricula

- Coordination of the content of the basic and advanced curricula

- Coordination of the content-related activities of lesson translation
{: .mb-5}
